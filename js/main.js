/*global $ */
/*eslint-env browser*/
$(document).ready(function () {
    'use strict';
    var parallax_effect = $('.parallax');
    parallax_effect.jarallax({
        speed: 0.5
    });
});


//========================Load=====================//
$(document).ready(function () {
    "use strict";
    $(window).on('load',function () {
        $(".load").fadeOut(100, function () {
            $("body").css("overflow", "auto");
            $(this).fadeOut(100, function () {
                $(this).remove();
            });
        });
    });

});

//========================Sycn=====================//
$(document).ready(function () {
    'use strict';
    $(window).click(function() {
        $("nav .collapse").removeClass("show");
    });
    
    $('.collapse').click(function(event){
        event.stopPropagation();
    });
     $("nav li a").click(function (e){

           e.preventDefault();
            $("html, body").animate({
                scrollTop: $("#"+ $(this).data("scroll")).offset().top - 70
            }, 1000);
          //Add Class Active
//          $(this).addClass("active").parent().siblings().find("a").removeClass("active");
       });
        // Sycn Navbar
    
        $(window).scroll(function (){
            $("section").each(function (){
                if($(window).scrollTop() > ($(this).offset().top - 90)){
                    var sectionID=$(this).attr("id");
                    $("nav li a ").removeClass("active");
                    $("nav li a[data-scroll='"+ sectionID +"']").addClass("active");
                }
            })
        })
});
//========================Scroll To Top=====================//
$(document).ready(function (){

  var scrollButton=$("#scroll");

  $(window).scroll(function(){
      if($(this).scrollTop()>=700)
      {
          scrollButton.show();
      }
      else
      {
          scrollButton.hide();
      }
  });
  scrollButton.click(function(){
      $("html,body").animate({scrollTop:0},900);
  });
});
//=======================Slider=====================//
$(document).ready(function (){

    $(".home .slider").slick({
        rtl: true,
        nextArrow:".sliderControl .arrow.next",
        prevArrow:".sliderControl .arrow.prev",
        dots:false,
//        autoplay: true,
//        autoplaySpeed: 1500,

        
        
    });
}); 